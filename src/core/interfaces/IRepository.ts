import { IOwner } from './IOwner';

export interface IRepository {
  name: string;
  full_name: string;
  description: string;
  forks_count: number; //number of stars
  open_issues_count: number; //number of issues
  created_at: Date;
  updated_at: Date;
  pushed_at: Date;
  owner: IOwner;
}
