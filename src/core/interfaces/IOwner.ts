export interface IOwner {
  login: string; //owner user name
  avatar_url: string;
}
