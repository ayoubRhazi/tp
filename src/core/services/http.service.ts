import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient) {}

  substractDays(days: number): string {
    let date = new Date();
    let newDate: string;

    date.setDate(date.getDate() - days);
    let month = date.getUTCMonth() + 1; //months from 1-12
    let day = date.getUTCDate();
    let year = date.getUTCFullYear();

    newDate = year + '-' + month + '-' + day;
    return newDate;
  }

  getRequest(url): any {
    // this.http.get(url).subscribe((response) => {
    //   console.log(response);
    // });
    return this.http.get(url);
  }
}
