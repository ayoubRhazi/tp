import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class RepositoryServiceService {
  constructor(private http: HttpClient) {}
  substractDays(days: number): string {
    let date = new Date();
    let newDate: string;

    date.setDate(date.getDate() - days);
    let month = date.getUTCMonth() + 1; //months from 1-12
    let day = date.getUTCDate();
    let year = date.getUTCFullYear();

    newDate = year + '-' + month + '-' + day;
    return newDate;
  }
  getRepository(id): any {
    const url = 'https://api.github.com/repositories/' + id;
    return this.http.get(url);
  }
}
