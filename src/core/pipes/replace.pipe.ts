import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replace',
})
export class ReplacePipe implements PipeTransform {
  transform(value: string) {
    console.log(value);

    return value.replace('/', ' ');
  }
}
