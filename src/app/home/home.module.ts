import { RepoDetailComponent } from './repo-detail/repo-detail.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ReplacePipe } from 'src/core/pipes/replace.pipe';

@NgModule({
  declarations: [HomeComponent, RepoDetailComponent, ReplacePipe],
  imports: [CommonModule, HomeRoutingModule, NgbModule],
})
export class HomeModule {}
