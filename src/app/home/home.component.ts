import { IRepository } from './../../core/interfaces/IRepository';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from 'src/core/services/http.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  repositories: IRepository[];
  subscription: Subscription;
  page = 1;
  pageSize = 10;
  constructor(private httpService: HttpService) {}

  ngOnInit(): void {
    this.handleService();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  handleService() {
    const date = this.httpService.substractDays(30);
    const url =
      'https://api.github.com/search/repositories?q=created:%3E' +
      date +
      '&sort=stars&order=desc';

    this.subscription = this.httpService
      .getRequest(url)
      .subscribe((response) => {
        this.repositories = response.items;
        //this.totalRecords = response.items.length;
        //console.log(this.repositories.length);
      });
  }
}
