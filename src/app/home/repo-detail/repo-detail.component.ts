import { RepositoryServiceService } from 'src/core/services/repository-service.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IRepository } from '../../../core/interfaces/IRepository';
import { Location } from '@angular/common';

@Component({
  selector: 'app-repo-detail',
  templateUrl: './repo-detail.component.html',
  styleUrls: ['./repo-detail.component.css'],
})
export class RepoDetailComponent implements OnInit {
  repository: IRepository;

  constructor(
    private route: ActivatedRoute,
    private repoService: RepositoryServiceService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getRepo();
  }

  getRepo(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    //console.log(id);
    this.repoService.getRepository(id).subscribe((repository) => {
      this.repository = repository;
      //console.log(repository.created_at);
      // console.log(this.repository);
    });
  }
  goBack(): void {
    this.location.back();
  }
}
